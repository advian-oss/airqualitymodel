mkdir -p ~/.streamlit/
echo "[general]
email = \"matti.karppanen@advian.fi\"
" > ~/.streamlit/credentials.toml
echo "[server]
headless = true
port = $PORT
enableCORS = false
" > ~/.streamlit/config.toml
