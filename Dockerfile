# syntax=docker/dockerfile:1.1.7-experimental
#############################################
# Tox testsuite for multiple python version #
#############################################
FROM advian/tox-base:alpine as tox
# Set the used python versions (also install system level deps here
RUN pyenv global 3.8.1 3.7.6 \
    && true

######################
# Base builder image #
######################
FROM python:3.7-slim-buster as builder_base

SHELL ["/bin/bash", "-lc"]

ENV \
  # locale
  LC_ALL=C.UTF-8 \
  # python:
  PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  # pip:
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  # poetry:
  POETRY_VERSION=1.0.5


RUN apt-get update \
    && apt-get install -y \
        curl \
        git \
        libffi-dev \
        linux-headers-amd64 \
        openssl \
        libssl-dev \
        # zeromq3 \
        tini \
        openssh-client


ARG USER_ID
ARG GROUP_ID
RUN addgroup --gid $GROUP_ID user \
    && adduser --disabled-password --gecos '' --uid $USER_ID --gid $GROUP_ID user
USER user


# githublab ssh
RUN mkdir -p -m 0700 ~/.ssh && ssh-keyscan gitlab.com github.com | sort > ~/.ssh/known_hosts \
    # Installing `poetry` package manager:
    # https://github.com/python-poetry/poetry
    #&& pip3 install "poetry==$POETRY_VERSION" \
    # Workaround problem with pypi poetry and datastreamservicelib requiring conflicting tomlkit
    # (would mess up the requirements.txt workaround below).
    && curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python \
    && echo 'source $HOME/.poetry/env' >> $HOME/.profile \
    && source $HOME/.poetry/env \
    # We're in a container, do not create useless virtualenvs
    && poetry config virtualenvs.create false \
    && true


# Copy only requirements, to cache them in docker layer:
WORKDIR pysetup
COPY ./poetry.lock ./pyproject.toml /pysetup
USER root
RUN chown -R user:user /pysetup
USER user
# Install basic requirements
# But only after https://github.com/python-poetry/poetry/issues/2102 is fixed
#RUN --mount=type=ssh poetry config virtualenvs.create false \
#  && poetry install --no-dev --no-interaction --no-ansi
# Workaround by exporting deps, building and installing them via pip
RUN --mount=type=ssh pip install wheel \
    && poetry export -f requirements.txt --without-hashes -o /tmp/requirements.txt \
    && pip wheel --wheel-dir=/tmp/wheelhouse -r /tmp/requirements.txt \
    # TODO: Enable the install line when you actually have requirements
    #&& pip3 install /tmp/wheelhouse/*.whl \
    && true


####################################
# Base stage for production builds #
####################################
FROM builder_base as production_build
# Copy entrypoint script
COPY ./docker/entrypoint.sh /docker-entrypoint.sh
# Only files needed by production setup
COPY ./poetry.lock ./pyproject.toml ./src /app/
WORKDIR /app
# Build the wheel package with poetry and add it to the wheelhouse
RUN --mount=type=ssh poetry build -f wheel --no-interaction --no-ansi \
    && cp dist/*.whl /tmp/wheelhouse \
    && chmod a+x /docker-entrypoint.sh \
    && true


#########################
# Main production build #
#########################
FROM python:3.7-slim-buster as production
COPY --from=production_build /tmp/wheelhouse /tmp/wheelhouse
COPY --from=production_build /docker-entrypoint.sh /docker-entrypoint.sh
WORKDIR /app
# Install system level deps for running the package (not devel versions for building wheels)
# and install the wheels we built in the previous step. generate default config
RUN apt-get update \
    && apt-get install -y tini \
    && chmod a+x /docker-entrypoint.sh \
    && pip install /tmp/wheelhouse/*.whl \
    && rm -rf /tmp/wheelhouse/ \
    # Do whatever else you need to
    && true
ENTRYPOINT ["/sbin/tini", "--", "/docker-entrypoint.sh"]


#####################################
# Base stage for development builds #
#####################################
FROM builder_base as devel_build
# Install deps
WORKDIR /pysetup
RUN --mount=type=ssh poetry install --no-interaction --no-ansi \
    && true

###########
# Hacking #
###########
FROM devel_build as devel_shell
# Copy everything to the image
COPY . /app
WORKDIR /app
USER root
RUN apt-get update \
    && apt-get install -y zsh \
    && su - user -c 'sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"' \
    && echo "source /home/user/.profile" >>/home/user/.zshrc \
    && true
USER user
ENTRYPOINT ["/bin/zsh", "-l"]
