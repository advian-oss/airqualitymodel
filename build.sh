#!/bin/bash
#
# Build the air quality app on heroku.
#
# Please provide the app name as the first cli argument.

rm -rf build \
&& mkdir -p build \
&& cd build \
&& git init \
&& heroku git:remote -a $1 \
&& git pull heroku master \
&& cp ../heroku/* . \
&& mkdir airqualitymodel data models \
&& cp ../src/airqualitymodel/*.py airqualitymodel/ \
&& cp -r ../data/cleaned data/ \
&& cp ../models/airqualitymodel.sav models/ \
&& git add . \
&& git commit -m "deployment" \
&& heroku maintenance:on \
&& git push heroku master \
&& heroku maintenance:off \
&& true
