"""Feature selection."""

import click
import pandas as pd
import numpy as np

from airqualitymodel.config import CATEGORICAL_VARIABLES


def select_features(data: pd.DataFrame, importance: pd.DataFrame, num_features: int, max_corr: float) -> pd.DataFrame:
    """Feature selection based of feature importances

    Step One: If the correlation between two features exceeds `max_corr`, drop the less important one.
    Step Two: Keep the `num_features` most important of the remaining features after step one.
    """

    assert max_corr >= 0.7
    assert max_corr < 1
    assert num_features > 0
    assert num_features <= data.shape[1]

    cont_data = data.drop(columns=CATEGORICAL_VARIABLES)
    feature_rank = rank_features(importance)
    cont_feature_rank = feature_rank.loc[cont_data.columns].sort_values(by="Value")

    corrs = feature_correlations(cont_data, cont_feature_rank)

    high_corr_features_to_drop = set(corrs.loc[corrs.value > max_corr, "variable"])

    remaining_after_step_one = set(data.columns) - high_corr_features_to_drop
    remaining_after_step_two = (
        feature_rank.loc[remaining_after_step_one].sort_values("Value").index.to_list()[:num_features]
    )

    return remaining_after_step_two


def feature_correlations(data: pd.DataFrame, feature_ranking: pd.Series) -> pd.DataFrame:
    """Create a data frame with feature pairs and their Pearson correlation.

    The index of the returned data frame has the higher ranked variable of the two."""

    corr_matrix = data[feature_ranking.index].corr()
    corr_matrix = corr_matrix.dropna(how="all").dropna(axis=1, how="all")

    # Upper triangle of the correlation matrix has all the data we need.
    corr_matrix = pd.DataFrame(np.triu(corr_matrix, k=1), index=corr_matrix.index, columns=corr_matrix.columns).replace(
        0, np.nan
    )

    return corr_matrix.melt(ignore_index=False).dropna()


def rank_features(attribs: pd.DataFrame) -> pd.Series:
    """Rank features based on summed up feature attributions."""
    return attribs.set_index("Feature")[["Value"]].abs().groupby(level=0).sum().rank(ascending=False)


@click.command()
@click.option("-f", "--num-features", type=int, default=50, help="Number of remaining features after selection.")
@click.option("-c", "--max-corr", type=float, default=0.99, help="Maximum pairwise correlation allowed.")
def main(num_features: int, max_corr: float) -> None:
    """Run feature selection with existing model and features, overwriting feature files."""
    x_train = pd.read_csv("data/enriched/X_train.csv", index_col="Datetime")
    x_test = pd.read_csv("data/enriched/X_test.csv", index_col="Datetime")
    attribs = pd.read_csv("data/curated/feature_attributions.csv")

    remaining_features = select_features(x_test, attribs, num_features=num_features, max_corr=max_corr)

    x_train[remaining_features].to_csv("data/enriched/X_train.csv")
    x_test[remaining_features].to_csv("data/enriched/X_test.csv")


if __name__ == "__main__":
    main()  # pylint: disable=no-value-for-parameter
