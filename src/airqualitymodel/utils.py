"""Utility functions"""
import pandas as pd

import torch
import torch.nn.functional as F

from airqualitymodel.model import PerceptronWithEmbeddings


def datetime(data: pd.DataFrame) -> pd.Series:
    """Convert Year, Month, Day, and Time (hhmm) columns to datetime."""
    hours = pd.to_datetime(data["Time"]).dt.strftime("%H:%M:%S")
    hours = pd.to_timedelta(hours)
    return pd.to_datetime(data[["Day", "Month", "Year"]]) + hours


def forward(x_cat: torch.Tensor, x_cont: torch.Tensor, model: PerceptronWithEmbeddings) -> torch.Tensor:
    """Separate forward function without embedding layers for model interpretation to be used with Captum"""
    out = x_cat
    out = model.embedding_dropout(out)

    x_cont_normalized = model.first_bn_layer(x_cont)
    out = torch.cat([out, x_cont_normalized], 1)

    for linear_layer, dropout_layer, bn_layer in zip(model.linear_layers, model.dropout_layers, model.bn_layers):
        out = F.relu(linear_layer(out))
        out = bn_layer(out)
        out = dropout_layer(out)

    out = model.output_layer(out)

    return out
