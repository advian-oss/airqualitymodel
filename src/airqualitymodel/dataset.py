"""Helper class for tabular loading data"""
# pylint: disable=not-callable
from typing import Tuple, Union, List, Any
from typing import TYPE_CHECKING

import numpy as np
import pandas as pd

import torch
from torch.utils.data import Dataset


if TYPE_CHECKING:
    BaseDataset = Dataset[Any]  # pylint: disable=unsubscriptable-object
else:
    BaseDataset = Dataset


class TabularDataset(BaseDataset):
    """Tabular dataset for PyTorch"""

    def __init__(
        self, features: pd.DataFrame, target: Union[None, pd.DataFrame] = None, cat_cols: Union[None, List[str]] = None
    ):
        self.features_cont_names = features.drop(columns=cat_cols).copy().columns
        self.features_cont = torch.tensor(features.drop(columns=cat_cols).copy().values.astype(np.float32))

        if cat_cols is not None:
            self.features_cat = torch.tensor(features.loc[:, cat_cols].copy().values.astype(np.int64))
        else:
            self.features_cat = torch.empty(len(self))

        if target is not None:
            self.target = torch.tensor(target.copy().values.astype(np.float32))
        else:
            self.target = torch.empty(len(self))

    def __len__(self) -> int:
        return self.features_cont.shape[0]

    def __getitem__(self, item: int) -> Tuple[torch.Tensor, torch.Tensor, torch.Tensor]:
        return self.features_cat[item], self.features_cont[item], self.target[item]
