"""Config vars."""
TARGET_VARIABLES = [
    "PM2.5_Makelankatu",
    "PM2.5_Lansisatama",
    "PM2.5_Pirkkola",
    "PM2.5_Kallio",
    "PM2.5_Mannerheimintie",
]
CATEGORICAL_VARIABLES = ["Month", "Weekday", "Hour"]

# Data paths
DATA_PATH = "data"
RAW_DATA_PATH = "data/raw"
CLEANED_DATA_PATH = "data/cleaned"
ENRICHED_DATA_PATH = "data/enriched"
CURATED_DATA_PATH = "data/curated"
MODEL_PATH = "models/air_quality_model.pt"
PIPELINE_PATH = "models/sklearn_pipeline.sav"
