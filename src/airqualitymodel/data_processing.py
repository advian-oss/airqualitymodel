#!/usr/bin/env python
"""Data processing"""
from typing import Dict

import os
from pathlib import Path

import pandas as pd
from sklearn.model_selection import train_test_split

from airqualitymodel import utils, feature_engineering, LOGGER
from airqualitymodel.config import TARGET_VARIABLES, RAW_DATA_PATH, CLEANED_DATA_PATH, ENRICHED_DATA_PATH


class InputData:
    """Class for the different types of input data."""

    variables: Dict[str, str]
    date_columns: Dict[str, str]
    data: pd.DataFrame

    def __init__(self, variables: Dict[str, str]):
        self.variables = variables
        self.date_columns = {
            "Vuosi": "Year",
            "Kk": "Month",
            "Pv": "Day",
            "Klo": "Time",
        }

    def read_dir(self, directory: str) -> pd.DataFrame:
        """Loads csv files from a directory and combines the variable names with locations"""
        parquets = list(Path(directory).rglob("*.parquet"))
        csvs = list(Path(directory).rglob("*.csv"))
        dfs = [self.read_location_file(f) for f in parquets + csvs]
        self.data = pd.concat(dfs, axis=1, join="outer")

    def read_location_file(self, file_path: Path) -> pd.DataFrame:
        """Read a csv file and rename columns based on location.
        Assume the stem of the filename is the location.
        Return a dataframe with an index consisting of all time-related columns.
        """
        columns_to_read = {**self.date_columns, **self.variables}.keys()
        loc = file_path.stem.split("_")[0:-2]
        rename_dict = self.rename_dict("_".join(loc))

        if file_path.suffix == ".parquet":
            data = pd.read_parquet(file_path, columns=columns_to_read)
        else:
            data = pd.read_csv(file_path, usecols=columns_to_read)

        data = data.rename(columns=rename_dict).drop_duplicates()

        data["Datetime"] = utils.datetime(data)
        data = data.drop(columns=list(self.date_columns.values()))
        return data.set_index("Datetime")

    def rename_dict(self, loc: str) -> Dict[str, str]:
        """Concat location info to variable names."""
        var_renames = {k: "_".join([v, loc.capitalize()]) for k, v in self.variables.items()}
        return {**self.date_columns, **var_renames}


def main() -> None:
    """Data processing pipeline."""
    air_quality_data = InputData(
        {
            "Hiilimonoksidi (ug/m3)": "CO",
            "Typpidioksidi (ug/m3)": "NO2",
            "Otsoni (ug/m3)": "O3",
            "Rikkidioksidi (ug/m3)": "SO2",
            "Pienhiukkaset (ug/m3)": "PM2.5",
        }
    )

    weather_data = InputData(
        {
            "Pilvien määrä (1/8)": "Clouds",
            "Ilmanpaine (msl) (hPa)": "AirPressure",
            "Suhteellinen kosteus (%)": "Humidity",
            "Sateen intensiteetti (mm/h)": "RainIntensity",
            "Lumensyvyys (cm)": "SnowDepth",
            "Ilman lämpötila (degC)": "AirTemperature",
            # "Kastepistelämpötila (degC)": "DewPointTemperature",
            "Näkyvyys (m)": "Visibility",
            "Tuulen suunta (deg)": "WindDirection",
            # "Puuskanopeus (m/s)": "GustSpeed",
            "Tuulen nopeus (m/s)": "WindSpeed",
        }
    )

    traffic_data = InputData({"HA": "Car", "KA": "Truck", "Linja-autot": "Bus",})

    vegetation_data = InputData({"VegetationIndex": "VegetationIndex"})

    air_quality_data.read_dir(os.path.join(RAW_DATA_PATH, "AirQuality"))
    weather_data.read_dir(os.path.join(RAW_DATA_PATH, "Weather"))
    traffic_data.read_dir(os.path.join(RAW_DATA_PATH, "Traffic"))
    vegetation_data.read_dir(os.path.join(RAW_DATA_PATH, "VegetationIndex"))

    # Combine data sources
    data = pd.concat([air_quality_data.data, weather_data.data, traffic_data.data, vegetation_data.data], axis=1)

    data = feature_engineering.implement(data)

    # Drop all NA columns, and zero variance columns
    data = data.loc[:, data.std() > 0].dropna(axis=1, how="all")

    # Split data
    target = data[TARGET_VARIABLES]
    # data = data.drop(columns=TARGET_VARIABLES)

    dataset_txt = f"Dataset contains {data.shape[0]} examples, {data.shape[1]} features and {target.shape[1]} targets."
    LOGGER.info(dataset_txt)

    # Have y be one hour ahead compared to x
    data = data.iloc[:-1]
    data_index = data.index
    target = target.iloc[1:].set_index(data_index)

    LOGGER.info("Finished feature engineering. Exporting the data to the path: {}.".format(CLEANED_DATA_PATH))
    data.to_csv(os.path.join(CLEANED_DATA_PATH, "data.csv"), index=True)
    target.to_csv(os.path.join(CLEANED_DATA_PATH, "target.csv"), index=True)

    # Drop rows where any target is missing
    mask = target.notna().all(axis=1)
    target = target.loc[mask]
    data = data.loc[mask]

    x_train, x_test, y_train, y_test = train_test_split(data, target, test_size=0.1, shuffle=False)

    # Export data
    LOGGER.info("Finished splitting the data. Exporting the splits to the path: {}.".format(ENRICHED_DATA_PATH))
    for i, name in zip((x_train, y_train, x_test, y_test), ["X_train", "y_train", "X_test", "y_test"]):
        i.to_csv(os.path.join(ENRICHED_DATA_PATH, str(name + ".csv")), index=True)


if __name__ == "__main__":
    main()
