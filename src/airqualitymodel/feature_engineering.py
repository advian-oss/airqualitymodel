"""Feature engineering functions."""
from typing import Sequence

import pandas as pd
import numpy as np
import holidays


def implement(data: pd.DataFrame) -> pd.DataFrame:
    """Feature engineering pipeline."""
    data = replace_negative_values_in_particle_concentration_with_zero(data)
    data = replace_sentinel_with_nan_in_wind_columns(data)
    data = replace_sentinel_with_nan_in_snow_depth_columns(data)
    data = decompose_wind_direction(data)

    # diff_df = multiple_diff(data, stepsizes=[1,3,6,12,24])
    # data = multiple_rolling(data, windows=[3,6,12,24], functions=["mean"])
    # data = pd.concat((data, diff_df), axis=1)

    data = data.drop(
        columns=["AirTemperature_Malmi", "AirTemperature_Harmaja", "AirTemperature_Vuosaari", "AirTemperature_Kumpula"]
    )

    temperature_features = data.columns[data.columns.str.contains("Temperature")].to_list()
    data = replace_feature_with_24h_mean_and_change(data, temperature_features)

    data = data.iloc[24:]

    return time_feature_engineering(data)


def replace_feature_with_24h_mean_and_change(data: pd.DataFrame, features: Sequence[str]) -> pd.DataFrame:
    """Calculate the 24h average and change in columns. Delete origin columns."""
    changes = multiple_diff(data[features], [24])
    daily_means = multiple_rolling(data[features], [24], ["mean"])

    return pd.concat([data, changes, daily_means], axis=1).drop(columns=features)


def multiple_diff(data: pd.DataFrame, stepsizes: Sequence[int]) -> pd.DataFrame:
    """Calculate changes for multiple stepsizes for all columns."""
    diff_dfs = (
        data.diff(i).rename({col: f"{col}_DiffTo{i}HoursAgo".format(col, i) for col in data.columns}, axis=1)
        for i in stepsizes
    )
    return pd.concat(diff_dfs, axis=1)


def multiple_rolling(data: pd.DataFrame, windows: Sequence[int], functions: Sequence[str]) -> pd.DataFrame:
    """Calculate rolling aggregates for multiple functions in multiple windows."""
    rolling_dfs = (
        data.rolling(i).agg(functions).rename({col: f"{col}_{i}Hours" for col in data.columns}, axis=1) for i in windows
    )
    df_out = pd.concat(rolling_dfs, axis=1)
    df_out.columns = ["_".join(col).strip() for col in df_out.columns.values]
    return df_out


def time_feature_engineering(data: pd.DataFrame) -> pd.DataFrame:
    """Engineer features related to time and date."""
    data = data.reset_index()

    data["Year"] = data["Datetime"].dt.year
    data["Month"] = data["Datetime"].dt.month
    # data["Day"] = data["Datetime"].dt.day
    data["Hour"] = data["Datetime"].dt.hour

    data["Weekday"] = data["Datetime"].dt.weekday
    data["Weekend"] = np.where((data["Weekday"] == 5) | (data["Weekday"] == 6), 1, 0)

    fin_holidays = holidays.Finland(years=data["Year"].unique())
    data["Holiday"] = data["Datetime"].isin(fin_holidays).astype(int)

    return data.set_index("Datetime").drop(["Year"], axis=1)


def replace_sentinel_with_nan(
    data: pd.DataFrame, columns: Sequence[str], sentinel: float, replace_with: float = np.nan
) -> pd.DataFrame:
    """Replace sentinel value with NAN in a subset of columns."""
    data[columns] = data[columns].replace(sentinel, replace_with)
    return data


def replace_negative_values_in_particle_concentration_with_zero(data: pd.DataFrame) -> pd.DataFrame:
    """PM2.5 has some negative values. Replace with zeroes."""
    microparticle_columns = data.columns[data.columns.str.startswith(r"PM2.5")]
    data[microparticle_columns] = data[microparticle_columns].clip(lower=0)
    return data


def replace_sentinel_with_nan_in_wind_columns(data: pd.DataFrame) -> pd.DataFrame:
    """The WindSpeed and GustSpeed columns have zeroes as sentinel values. Replace with np.nan."""
    windspeed_columns = data.columns[data.columns.str.contains(r"(Gust|Wind)")]
    return replace_sentinel_with_nan(data, windspeed_columns, 0)


def replace_sentinel_with_nan_in_snow_depth_columns(data: pd.DataFrame) -> pd.DataFrame:
    """The SnowDepth columns use -1 as zero in the summer. Replace with 0."""
    snow_depth_columns = data.columns[data.columns.str.startswith("SnowDepth")]
    return replace_sentinel_with_nan(data, snow_depth_columns, sentinel=-1, replace_with=0)


def decompose_wind_direction(data: pd.DataFrame) -> pd.DataFrame:
    """Decompose WindDirection into North-South and East-West components."""
    wind_direction_columns = data.columns[data.columns.str.contains(r"WindDirection")]
    for col in wind_direction_columns:
        spot = col.split("_")[-1]
        data[f"WindDirectionWestEast_{spot}"] = np.cos(np.radians(data[col]))
        data[f"WindDirectionSouthNorth_{spot}"] = np.sin(np.radians(data[col]))
        data = data.drop(columns=col)
    return data
