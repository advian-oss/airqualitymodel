"""Air Quality Prediction Model"""
from typing import Any, Tuple, Sequence, List, Dict, Type, TypeVar, TYPE_CHECKING

import copy

import pandas as pd
import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import DataLoader

from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import PowerTransformer, OrdinalEncoder, MinMaxScaler
from sklearn.experimental import enable_iterative_imputer  # pylint: disable=unused-import
from sklearn.impute import IterativeImputer
from sklearn.pipeline import make_pipeline, Pipeline

from airqualitymodel import LOGGER
from airqualitymodel.dataset import TabularDataset
from airqualitymodel.config import CATEGORICAL_VARIABLES

AQM = TypeVar("AQM", bound="AirQualityModel")

if TYPE_CHECKING:
    BaseDataLoader = DataLoader[Any]  # pylint: disable=unsubscriptable-object
else:
    BaseDataLoader = DataLoader


class PerceptronWithEmbeddings(nn.Module):  # pylint: disable=too-many-instance-attributes, abstract-method
    """Feed-forward Neural Network with categorical embedding layers"""

    def __init__(  # pylint: disable=too-many-arguments
        self,
        embedding_sizes: Sequence[Tuple[int, int]],
        n_cont: int,
        linear_layer_sizes: List[int],
        embedding_dropout: float,
        linear_layer_dropouts: List[float],
        output_size: int,
    ) -> None:
        super().__init__()  # type: ignore

        assert len(linear_layer_sizes) == len(
            linear_layer_dropouts
        ), "The number of linear layers and the number of dropout ratios don't match."

        # Embedding Layers
        self.embeddings = nn.ModuleList([nn.Embedding(categories, size) for categories, size in embedding_sizes])
        n_emb = sum(e.embedding_dim for e in self.embeddings)

        # Linear Layers
        input_layer = nn.Linear(n_emb + n_cont, linear_layer_sizes[0])
        self.linear_layers = nn.ModuleList(
            [input_layer]
            + [nn.Linear(linear_layer_sizes[i], linear_layer_sizes[i + 1]) for i in range(len(linear_layer_sizes) - 1)]
        )

        # Output Layer
        self.output_layer = nn.Linear(linear_layer_sizes[-1], output_size)

        # Batch Norm Layers
        self.first_bn_layer = nn.BatchNorm1d(n_cont)  # type: ignore
        self.bn_layers = nn.ModuleList([nn.BatchNorm1d(size) for size in linear_layer_sizes])  # type: ignore

        # Dropout Layers
        self.embedding_dropout = nn.Dropout(embedding_dropout)
        self.dropout_layers = nn.ModuleList([nn.Dropout(size) for size in linear_layer_dropouts])

        # Kaiming weight initialization
        for linear_layer in self.linear_layers:
            nn.init.kaiming_normal_(linear_layer.weight.data)  # type: ignore
        nn.init.kaiming_normal_(self.output_layer.weight.data)  # type: ignore

    def forward(self, x_cat: torch.Tensor, x_cont: torch.Tensor) -> Any:
        """Forward propagation"""
        out = torch.cat([e(x_cat[:, i]) for i, e in enumerate(self.embeddings)], dim=1)
        out = self.embedding_dropout(out)

        x_cont_normalized = self.first_bn_layer(x_cont)
        out = torch.cat([out, x_cont_normalized], 1)

        for linear_layer, dropout_layer, bn_layer in zip(self.linear_layers, self.dropout_layers, self.bn_layers):
            out = F.relu(linear_layer(out))
            out = bn_layer(out)
            out = dropout_layer(out)

        out = self.output_layer(out)

        return out


class AirQualityModel:
    """Air Quality Model.

    The `x_train` argument in the intitializer is only used to calculate categorical feature cardinalities.
    """

    def __init__(
        self, transformer: Pipeline, selected_features: Sequence[str], x_train: pd.DataFrame, x_test: pd.DataFrame
    ):
        self.categorical_features = [ft for ft in selected_features if ft in CATEGORICAL_VARIABLES]

        # Encode categorical variables
        categorical_feature_cardinalities = [x_train[name].nunique() for name in self.categorical_features]
        embedding_sizes = [(m, int(m ** 0.5)) for m in categorical_feature_cardinalities]

        # Create neural network
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.model = PerceptronWithEmbeddings(
            embedding_sizes=embedding_sizes,
            n_cont=len(selected_features) - len(self.categorical_features),
            linear_layer_sizes=[200, 100],
            linear_layer_dropouts=[0.3, 0.3],
            embedding_dropout=0.1,
            output_size=5,
        ).to(device)

        self.transformer = transformer
        self.column_order = get_column_order(transformer)
        self.selected_features = selected_features
        self.target_list = x_test.columns.to_list()

    def transform(self, data: pd.DataFrame) -> pd.DataFrame:
        """Transform the data using a fitted transformation pipeline.

        In addition to keeping directory structure, add indicator columns names as {colname}_Missing.
        """
        result = self.transformer.transform(data)
        return pd.DataFrame(result, columns=self.column_order, index=data.index).loc[:, self.selected_features]

    def train(  # pylint: disable=too-many-locals
        self, data: Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame, pd.DataFrame], hyperparameters: Dict[str, Any]
    ) -> None:
        """Train the neural network."""
        x_train, x_test, y_train, y_test = data
        early_stop_rounds = hyperparameters["early_stop_rounds"]

        # Transform data
        x_train = self.transform(x_train)
        x_test = self.transform(x_test)

        # Create datasets
        train_set = TabularDataset(x_train, y_train, cat_cols=self.categorical_features)
        test_set = TabularDataset(x_test, y_test, cat_cols=self.categorical_features)

        # Data loaders
        train_loader = DataLoader(train_set, batch_size=hyperparameters["batch_size"])
        test_loader = DataLoader(test_set, batch_size=len(test_set))

        # Loss function and optimizer
        criterion = torch.nn.MSELoss()
        optim = torch.optim.ASGD(self.model.parameters(), hyperparameters["learning_rate"], weight_decay=0.01)

        # Train model
        train_losses = []
        test_losses = []
        best_loss: float = (y_test * y_test).sum().sum()

        for epoch in range(1, hyperparameters["num_epochs"] + 1):
            train_loss = self._train_epoch(criterion, optim, train_loader)
            test_loss = self._eval(criterion, test_loader)
            train_losses.append(train_loss)
            test_losses.append(test_loss)

            if test_loss < best_loss:
                best_loss = test_loss
                best_model = copy.deepcopy(self.model.state_dict())
                best_epoch = epoch

            best_loss_txt = f"Best loss: {best_loss:.3f} (epoch {best_epoch})"
            epoch_txt = f"Epoch {epoch}, Train loss: {train_loss:.3f}, Test loss {test_loss:.3f}. \t {best_loss_txt}"
            LOGGER.info(epoch_txt)

            if best_epoch < len(test_losses) - early_stop_rounds + 1:
                LOGGER.info(
                    "Early stopping critetion met. The model has not improved in {} epochs.".format(early_stop_rounds)
                )
                self.model.load_state_dict(best_model)
                break

    def predict(self, x_test: pd.DataFrame, target: str) -> np.ndarray:
        """Calculate model predictions."""
        data = self.transform(x_test)
        dataset = TabularDataset(data, cat_cols=self.categorical_features)
        dataloader = DataLoader(dataset, batch_size=len(dataset))

        for cat, cont, _ in dataloader:  # only one item in iterable
            output = self.model(cat, cont)

        target_index = self.target_list.index(target)
        return output[:, target_index].detach().numpy()

    def _train_epoch(
        self, criterion: torch.nn.MSELoss, optim: torch.optim.Optimizer, train_loader: BaseDataLoader
    ) -> float:
        """Train model for a single epoch."""
        self.model.train()
        total_train = 0
        sum_loss_train = 0
        for cat, cont, target in train_loader:
            batch_size = target.shape[0]
            output = self.model(cat, cont)
            loss = criterion(output, target)
            optim.zero_grad()
            loss.backward()
            optim.step()
            total_train += batch_size
            sum_loss_train += batch_size * (loss.item())
        return sum_loss_train / total_train

    def _eval(self, criterion: torch.nn.MSELoss, test_loader: BaseDataLoader) -> float:
        """Evaluate model."""
        self.model.eval()
        total_test = 0
        sum_loss_test = 0
        for cat, cont, target in test_loader:
            batch_size = target.shape[0]
            output = self.model(cat, cont)
            loss = criterion(output, target)
            total_test += batch_size
            sum_loss_test += batch_size * (loss.item())
        return sum_loss_test / total_test

    @classmethod
    def from_data(cls: Type[AQM], x_train: pd.DataFrame, x_test: pd.DataFrame) -> AQM:
        """Create the instance wholly starting from data only."""
        LOGGER.info("Transforming the data...")

        uniform_features = x_train.columns[
            x_train.columns.str.contains(r"(RainIntensity|SnowDepth|Weekend|Holiday|Clouds)")
        ].to_list()
        visibility_features = x_train.columns[x_train.columns.str.startswith("Visibility")].to_list()

        numeric_features = x_train.drop(columns=CATEGORICAL_VARIABLES).columns.to_list()
        normal_features = (
            x_train[numeric_features].drop(columns=uniform_features + visibility_features).columns.to_list()
        )
        column_order = CATEGORICAL_VARIABLES + normal_features + uniform_features + visibility_features

        # Needed to silence SettingWithCopyWarning
        x_train = x_train.loc[:, column_order]

        column_transformer = ColumnTransformer(
            transformers=[
                ("enc", OrdinalEncoder(), CATEGORICAL_VARIABLES),
                ("normal", PowerTransformer(), normal_features),
                ("uniform", MinMaxScaler(), uniform_features),
                ("visibility", MinMaxScaler(feature_range=(-1, 0)), visibility_features),
            ]
        )
        missing_value_imputer = IterativeImputer(
            n_nearest_features=10, initial_strategy="most_frequent", verbose=2, add_indicator=True
        )

        data_transformer = make_pipeline(column_transformer, missing_value_imputer)
        data_transformer.fit(x_train)

        features = get_column_order(data_transformer)

        return cls(data_transformer, features, x_train, x_test)


def get_column_order(transformer: Pipeline) -> List[str]:
    """Correctly ordered column names after data transformation."""
    column_transformer = transformer.named_steps["columntransformer"]
    columns_of_transformer_parts = [part[2] for part in column_transformer.transformers]
    column_order = [item for sublist in columns_of_transformer_parts for item in sublist]

    iterative_imputer = transformer.named_steps["iterativeimputer"]
    indicator_columns = [f"{column_order[idx]}_Missing" for idx in iterative_imputer.indicator_.features_]
    column_order += indicator_columns
    return column_order
