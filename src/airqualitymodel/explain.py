#!/usr/bin/env python
"""Explain model predictions with Captum"""
from typing import List
import os

import pandas as pd
import numpy as np
from scipy import stats
import joblib
import torch
import click

from captum.attr import IntegratedGradients
from captum.attr import configure_interpretable_embedding_layer, remove_interpretable_embedding_layer

from airqualitymodel.utils import forward
from airqualitymodel.dataset import TabularDataset
from airqualitymodel.model import AirQualityModel
from airqualitymodel.config import CLEANED_DATA_PATH, ENRICHED_DATA_PATH, CURATED_DATA_PATH, CATEGORICAL_VARIABLES


def accumulated_local_effects(
    model: AirQualityModel, data: pd.DataFrame, feature: str, target: str, num_bins: int = 30
) -> pd.DataFrame:
    """Calculate Accumulated Local Effects (ALE) for a feature.

    https://christophm.github.io/interpretable-ml-book/ale.html
    """
    num_bins = min(num_bins, data[feature].nunique() - 1)

    quantiles = np.arange(0, 1 + 1 / num_bins, 1 / num_bins)
    bins = np.unique(stats.mstats.mquantiles(data[feature].dropna(), quantiles))
    bin_codes = pd.cut(data[feature], bins, right=False, include_lowest=True).cat.codes

    x_low = data.copy()
    x_high = data.copy()

    x_low[feature] = [bins[i] for i in bin_codes]
    x_high[feature] = [bins[i + 1] for i in bin_codes]

    y_low = model.predict(x_low, target)
    y_high = model.predict(x_high, target)

    changes = pd.DataFrame({feature: bins[bin_codes + 1], "delta": y_high - y_low})

    ale = changes.groupby([feature])["delta"].agg([("effect", "mean"), "size"])
    ale.loc[:, "effect"] = ale["effect"].cumsum()

    mean_effect = np.average(ale["effect"], weights=ale["size"])
    ale.loc[:, "effect"] = ale["effect"] - mean_effect

    return ale.sort_index()


@click.command()
@click.option("--absolute/--no-absolute", default=False, help="Sum absolute attributions?")
def main(absolute: bool) -> None:
    """Driver code"""
    model = joblib.load("models/airqualitymodel.sav")

    # Load data
    x_test = pd.read_csv(os.path.join(ENRICHED_DATA_PATH, "X_test.csv"), index_col="Datetime", header=0)
    y_test = pd.read_csv(os.path.join(ENRICHED_DATA_PATH, "y_test.csv"), index_col="Datetime", header=0)

    attributions = calculate_feature_attributions(model, x_test, y_test, absolute)

    # Export data
    attributions.to_csv(os.path.join(CURATED_DATA_PATH, "feature_attributions.csv"), index=False)


def calculate_feature_attributions(  # pylint: disable=too-many-locals
    aqm: AirQualityModel, features: pd.DataFrame, target: pd.DataFrame, absolute: bool = False
) -> pd.DataFrame:
    """Calculate feature attributions using integrated gradients.

    Integrated gradients are a fast gradient-based approximation of Shapley values for neural networks and other
    continuous gradient-based algorithms.

    https://captum.ai/docs/extension/integrated_gradients
    """

    transformed_data = aqm.transform(features)

    # Create dataset
    test_set = TabularDataset(transformed_data, target, cat_cols=aqm.categorical_features)

    model = aqm.model

    # Embedding inputs for Captum
    embedding_inp = [e(test_set.features_cat[:, i]) for i, e in enumerate(model.embeddings)]
    embeddings_cat = torch.cat(embedding_inp, 1)

    # Configure interpretable embedding layers
    interp_embeddings = []
    for i in range(len(model.embeddings)):
        interp_embeddings.append(configure_interpretable_embedding_layer(model, "embeddings." + str(i)))

    # Perform attributions using Integrated Gradients for all targets
    dfs = []
    igs = IntegratedGradients(forward)
    for i in range(model.output_layer.out_features):
        ig_attr_test = igs.attribute(  # pylint: disable=no-member
            inputs=(embeddings_cat, test_set.features_cont),
            additional_forward_args=model,
            target=i,
            return_convergence_delta=False,
            n_steps=1,
        )

        def attributions_sum_over_features(
            attributions: List[torch.Tensor], idx: int, sum_absolute_values: bool
        ) -> np.ndarray:
            attrs_at_index = attributions[idx]
            if sum_absolute_values:
                attrs_at_index = attrs_at_index.abs()
            return attrs_at_index.detach().numpy().sum(0)

        ig_attr_cat_sum = attributions_sum_over_features(ig_attr_test, idx=0, sum_absolute_values=absolute)
        ig_attr_cont_sum = attributions_sum_over_features(ig_attr_test, idx=1, sum_absolute_values=absolute)

        # Store attributions to dataframe
        categorical_df = categorical_attributions_dataframe(embedding_inp, ig_attr_cat_sum)
        continuous_df = pd.DataFrame(data={"Value": ig_attr_cont_sum, "Feature": test_set.features_cont_names})

        interpret_df = pd.concat([categorical_df, continuous_df], axis=0, ignore_index=True)
        interpret_df["Target"] = target.columns[i]

        # Append the dataframes to a list and concatenate dataframes
        dfs.append(interpret_df)

    df_out = pd.concat(dfs, axis=0, ignore_index=True)

    df_out = add_coordinates(df_out)

    # Remove Captum layers
    for single_interp_embed in interp_embeddings:
        remove_interpretable_embedding_layer(model, single_interp_embed)

    return df_out


def categorical_attributions_dataframe(embeddings: List[torch.Tensor], attribs: np.ndarray) -> pd.DataFrame:
    """Calculate the sums of the attributions of embedding dimensions by categorical variable."""
    embedding_dimensions = [e.shape[1] for e in embeddings]

    # The attributions were calculated for all embedded dimenstions.
    # We need a list of cat. feature names where that name is repeated [number of embedding dimensions] times.
    cat_names_list: List[str] = []
    for name, dim in zip(CATEGORICAL_VARIABLES, embedding_dimensions):
        cat_names_list += [name] * dim

    cat_df = pd.DataFrame(data={"Value": attribs, "Feature": cat_names_list})
    return cat_df.groupby("Feature").sum().reset_index()


def add_coordinates(data: pd.DataFrame) -> pd.DataFrame:
    """Add lat/lon to the locations of feature measurement spots."""
    loc_df = pd.read_csv(os.path.join(CLEANED_DATA_PATH, "locations.csv"))
    data["location"] = data.Feature.str.extract(r"_([A-Z][a-z_]+)$")
    return data.merge(loc_df, on="location", how="left")


if __name__ == "__main__":
    main()  # pylint: disable=no-value-for-parameter
