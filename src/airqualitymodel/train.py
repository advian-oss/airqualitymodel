#!/usr/bin/env python
"""Simple training script for the Air Quality Model"""
import os
from pathlib import Path

import click
import pandas as pd

# import matplotlib.pyplot as plt
import joblib

import torch

from airqualitymodel import explain, selection, LOGGER
from airqualitymodel.model import AirQualityModel
from airqualitymodel.config import DATA_PATH, MODEL_PATH


@click.command()
@click.option("--data-dir", type=click.Path(exists=True), default=Path(DATA_PATH, "enriched"))
@click.option("--batch-size", type=int, default=1024, help="Batch size for training")
@click.option("--learning-rate", type=float, default=0.0001, help="Learning rate")
@click.option("--epochs", type=int, default=4000, help="Number of epochs for model training")
@click.option("--early-stop-rounds", type=int, default=40, help="Number of epochs for early stopping")
def main(data_dir: str, batch_size: int, learning_rate: float, epochs: int, early_stop_rounds: int) -> None:
    """Entrypoint for training the Air Quality Model"""

    # Import data
    x_train = pd.read_csv(os.path.join(data_dir, "X_train.csv"), index_col="Datetime", header=0)
    x_test = pd.read_csv(os.path.join(data_dir, "X_test.csv"), index_col="Datetime", header=0)
    y_train = pd.read_csv(os.path.join(data_dir, "y_train.csv"), index_col="Datetime", header=0)
    y_test = pd.read_csv(os.path.join(data_dir, "y_test.csv"), index_col="Datetime", header=0)

    # Create model
    first_aqm = AirQualityModel.from_data(x_train, x_test)

    data = x_train, x_test, y_train, y_test
    hyperparameters = {
        "batch_size": batch_size,
        "learning_rate": learning_rate,
        "num_epochs": epochs,
        "early_stop_rounds": early_stop_rounds // 4,  # division only in the first round of training
    }

    first_aqm.train(data, hyperparameters)

    LOGGER.info("Finished training the preliminary model. Next we use if for feature selection.")

    attribs = explain.calculate_feature_attributions(first_aqm, features=x_test, target=y_test, absolute=True)

    top_features = selection.select_features(x_test, importance=attribs, num_features=50, max_corr=0.97)

    final_aqm = AirQualityModel(
        transformer=first_aqm.transformer, selected_features=top_features, x_train=x_train, x_test=x_test
    )

    hyperparameters["early_stop_rounds"] = early_stop_rounds

    final_aqm.train(data, hyperparameters)

    # Save model
    LOGGER.info("Finished training the model. Saving it to {}".format(MODEL_PATH))
    torch.save(final_aqm.model, MODEL_PATH)  # type: ignore
    joblib.dump(final_aqm, "models/airqualitymodel.sav")

    # Plot training and validation loss
    # epoch_range = range(1, len(test_losses) + 1)
    # plt.plot(epoch_range, train_losses, "g", label="Train Loss")
    # plt.plot(epoch_range, test_losses, "b", label="Test Loss")
    # plt.title("Train and Test Loss")
    # plt.xlabel("Epochs")
    # plt.ylabel("Loss")
    # plt.legend()
    # plt.show()


if __name__ == "__main__":
    main()  # pylint: disable=no-value-for-parameter
