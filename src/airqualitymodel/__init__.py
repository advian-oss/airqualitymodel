""" Deep Learning model for predicting airair quality """
__version__ = "0.1.0"  # NOTE Use `bump2version --config-file patch` to bump versions correctly

import logging

logging.basicConfig(level=logging.INFO)
LOGGER = logging.getLogger(__name__)
