#!/usr/bin/env python
"""Streamlit app for data visualization"""
from typing import Tuple, Any

import os

import streamlit as st
import pandas as pd
import joblib
import altair as alt
import pydeck as pdk
import waterfall_chart

from airqualitymodel import explain
from airqualitymodel.model import AirQualityModel
from airqualitymodel.config import CLEANED_DATA_PATH


def main() -> None:  # pylint: disable=too-many-locals
    """Driver code"""

    # Configure page layout
    st.beta_set_page_config(layout="centered")

    # Add title and text field for explaining what it's about
    st.title("Air Quality Prediction in Helsinki")
    st.markdown("This page visualizes...")

    aqm = get_model()
    data, labels = get_data()

    time_window_start = st.sidebar.date_input(
        "Time Window Start", value=pd.to_datetime("2020-07-01"), min_value=data.index[0], max_value=data.index[-1],
    )

    time_window_end = st.sidebar.date_input(
        "Time Window End", value=data.index[-1], min_value=data.index[0], max_value=data.index[-1],
    )

    if time_window_end < time_window_start:
        st.error("End date cannot be before start date.")

    data = get_time_window(data, time_window_start, time_window_end)
    labels = get_time_window(labels, time_window_start, time_window_end)

    attribs = get_attribs(aqm, data, labels)

    bar_chart_for_grouped_features(attribs)

    # Widget for selecting the target for visualization
    location = st.sidebar.selectbox("Location", ("Mäkelankatu", "Länsisatama", "Pirkkola", "Kallio", "Mannerheimintie"))

    predictions = get_predictions(aqm, data, location)

    ale_plot(aqm, data, location, attribs)

    waterfall_chart_single_obs(aqm, data, labels, predictions, location)

    st.header(f"The Most Important Features in {location}")
    top_n = st.slider("How many features", 1, get_num_features(attribs, location), 20)
    top_features = rank_top_features(attribs, location, top_n)
    bar_chart_for_important_features_with_sign(top_features)

    show_interactive_map(top_features)


def bar_chart_for_grouped_features(attribs: pd.DataFrame) -> None:
    """Draw a bar chart with the percent importance for features grouped by type."""

    totals = get_totals(attribs)

    # Chart for visualizing the aggregate importance of feature groups
    st.header("Most Important Feature Types")
    agg_chart = (
        alt.Chart(totals)
        .mark_bar()
        .encode(x="Percent", y=alt.Y("Feature", sort="-x"))
        .properties(width=1000, height=500)
        .configure_bar(fill="#002f49")
    )

    st.altair_chart(agg_chart, use_container_width=True)


def ale_plot(aqm: AirQualityModel, data: pd.DataFrame, location: str, attribs: pd.DataFrame) -> None:
    """ALE plot for a single feature in a single location."""
    st.header("Accumululated Local Effects")
    st.text(f"Draw an Accumululated Local Effects (ALE) plot for a single feature in {location}.")

    top_features = rank_top_features(attribs, location, n_features=10)

    target_name = get_target_column_name(location)
    ale_feature = st.selectbox("Select Feature:", options=top_features["Feature"][::-1].to_list())
    ales = explain.accumulated_local_effects(aqm, data, feature=ale_feature, target=target_name).reset_index()

    ale_chart = (
        alt.Chart(ales.reset_index().rename(columns={ale_feature: "feature"}))
        .mark_line()
        .encode(x=alt.X("feature", title=ale_feature), y="effect")
        .configure_line(color="#002f49")
    )
    st.altair_chart(ale_chart, use_container_width=True)


def bar_chart_for_important_features_with_sign(top_features: pd.DataFrame) -> None:
    """Chart for visualizing the 20 most important features for the selected target."""
    chart = (
        alt.Chart(top_features)
        .mark_bar()
        .encode(
            x="Value",
            y=alt.Y("Feature", sort="-x"),
            color=alt.condition(
                alt.datum.Value > 0,
                alt.value("steelblue"),  # The positive color
                alt.value("orange"),  # The negative color
            ),
        )
        .properties(width=1000, height=500)
    )

    st.altair_chart(chart, use_container_width=True)


def show_interactive_map(top_features: pd.DataFrame) -> None:
    """Map Visualization."""
    map_df = top_features.copy().dropna()
    map_df["AbsValue"] = map_df["Value"].abs()

    tooltip = {
        "html": "<b>Feature:</b> {Feature} <br/> <b>Attribution:</b> {Value}",
        "style": {"backgroundColor": "steelblue", "color": "white"},
    }

    deck = pdk.Deck(
        map_style="mapbox://styles/mapbox/light-v9",
        initial_view_state=pdk.ViewState(latitude=60.1733244, longitude=24.9410248, zoom=10, pitch=50),
        layers=[
            pdk.Layer(
                "ColumnLayer",
                data=map_df,
                get_position="[longitude, latitude]",
                radius=150,
                elevation_scale=20,
                get_elevation="AbsValue",
                elevation_range=[0, 10000],
                pickable=True,
                get_fill_color="[67 + 188 * (Value < 0), 192 + 41 * (Value > 0), 255 * (Value > 0)]",
                extruded=True,
            ),
        ],
        tooltip=tooltip,
    )

    st.pydeck_chart(deck)


def waterfall_chart_single_obs(
    aqm: AirQualityModel, data: pd.DataFrame, labels: pd.DataFrame, predictions: pd.Series, location: str
) -> None:
    """Waterfall plot for a single obs."""
    st.header(f"Interpret a single prediction in {location}.")

    obs_day = st.date_input("Choose Date:", value=data.index[-1], min_value=data.index[0], max_value=data.index[-1],)
    obs_hour = st.slider("Choose Hour:", min_value=0, max_value=23, value=0, step=1)
    obs_time = obs_day + pd.DateOffset(hours=obs_hour)

    obs = data.loc[obs_time].to_frame().T
    label = labels.loc[obs_time].to_frame().T
    pred = predictions[obs_time]

    obs_attribs = explain.calculate_feature_attributions(aqm, obs, label)
    obs_attribs = rank_top_features(obs_attribs, location, obs_attribs.shape[0])[::-1]

    st.write(f"Baseline_prediction: {pred - obs_attribs['Value'].sum():.1f}")
    st.write(f"Prediction: {pred:.1f}")
    st.write(f"Sum of attributions: {obs_attribs['Value'].sum():.1f}")

    waterfall_plot = waterfall_chart.plot(
        obs_attribs["Feature"],
        obs_attribs["Value"],
        threshold=0.3,
        rotation_value=45,
        green_color="#ff5d67",
        red_color="#43e9ff",
        blue_color="#323232",
    )
    st.pyplot(waterfall_plot)


@st.cache  # type: ignore
def get_data() -> Tuple[pd.DataFrame, pd.DataFrame]:
    """Load data from CSV files to cache."""
    data = pd.read_csv(os.path.join(CLEANED_DATA_PATH, "data.csv"), index_col="Datetime", parse_dates=["Datetime"])
    target = pd.read_csv(os.path.join(CLEANED_DATA_PATH, "target.csv"), index_col="Datetime", parse_dates=["Datetime"])
    return data, target


@st.cache  # type: ignore
def get_model() -> Any:
    """Load model."""
    return joblib.load("models/airqualitymodel.sav")


@st.cache  # type: ignore
def get_predictions(aqm: AirQualityModel, data: pd.DataFrame, location: str) -> pd.Series:
    """Cached predictions for all of the data."""
    preds = aqm.predict(data, get_target_column_name(location))
    return pd.Series(preds, index=data.index, name="AirQuality")


@st.cache  # type: ignore
def get_attribs(aqm: AirQualityModel, data: pd.DataFrame, target: pd.DataFrame) -> pd.DataFrame:
    """Load the CSV file to cache."""
    return explain.calculate_feature_attributions(aqm, data, target, absolute=False)


def get_time_window(data: pd.DataFrame, start_date: pd.datetime, end_date: pd.datetime) -> pd.DataFrame:
    """Filter data and target based on dates."""
    start_hour = pd.to_datetime(start_date)
    end_hour = pd.to_datetime(end_date) + pd.DateOffset(days=1)
    return data.loc[(data.index >= start_hour) & (data.index < end_hour)]


@st.cache  # type: ignore
def get_totals(data: pd.DataFrame) -> pd.DataFrame:
    """Get aggregate feature importance for all measuring spots and targets."""
    total = data.drop(columns=["Target", "location"]).set_index("Feature").abs().groupby(level=0)["Value"].mean()
    total = total / total.sum() * 100
    grouped = total.to_frame().reset_index()
    grouped["group"] = [ft.split("_")[0] for ft in grouped.Feature]
    grouped["loc"] = [ft.split("_")[-1] for ft in grouped.Feature]
    grouped = grouped.groupby("group")["Value"].sum().sort_values(ascending=False)
    return grouped.reset_index().rename(columns={"group": "Feature", "Value": "Percent"})


def get_target_column_name(location: str) -> str:
    """Parse location name into target column name."""
    return f"PM2.5_{descandinavify(location)}"


def get_num_features(data: pd.DataFrame, location: str) -> int:
    """Get the number of features in the location in the data."""
    return int(data.loc[data.Target == get_target_column_name(location), "Feature"].nunique())


def rank_top_features(data: pd.DataFrame, location: str, n_features: int = 20) -> pd.DataFrame:
    """Get the top features sorted by value."""
    location = get_target_column_name(location)
    return data.reindex(data["Value"].abs().sort_values().index).loc[data.Target == location].tail(n_features)


def descandinavify(text: str) -> str:
    """Remove the accents (¨) from scandinavian letters."""
    replace = {"Ä": "A", "ä": "a", "Ö": "O", "ö": "o", "Å": "a", "å": "a"}
    return "".join(replace.get(letter, letter) for letter in text)


if __name__ == "__main__":
    main()
