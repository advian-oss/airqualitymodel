#!/bin/bash
python src/airqualitymodel/data_processing.py
python src/airqualitymodel/train.py --epochs 600 --learning-rate .003
python src/airqualitymodel/explain.py
