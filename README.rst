=============================
AirQualityModel
=============================

Background
----------

The purpose of this project is to build a Deep Learning model for predicting and measuring the relative feature importances affecting local air quality in Helsinki, Finland.
The ultimate goal is to use this model to provide reliable and useful information for city residents on the different factors that affect local urban air quality (UAQ),
thereby creating awareness of the connected issues and promoting development towards better urban air quality.

The project relies on open source data to estimate interactions between features from different domains, such as measured air quality variables, waether and traffic data.
By doing so, it in other words strives to map how different local and temporal variables affect local UAQ, in it's part answering the question why the air quality varies
across time and space.

To enable communication of the results as widely as possible, a simple browser-based user interface is also initialized.

The project was enabled by cooperation with Forum Virium Helsinki via the UIA HOPE Innovation Contest in 2020.

Data
----

The data for the project is gathered from open sources provided by the Finnish Meteorological Institute (FMI) and Digitraffic.

Development
-----------

- Have python3.8 and virtualenvwrapper isntalled on your system.

- install Poetry: https://python-poetry.org/docs/#installation

- Create and activate a Python 3.7 virtualenv (assuming virtualenvwrapper)::

    mkvirtualenv -p `which python3.7` my_virtualenv

- Install dependencies::

    poetry install

Remember to activate your virtualenv whenever working on the repo, this is needed
because pylint and mypy pre-commit hooks use the "system" python for now (because reasons).::

    workon my_virtualenv

- Work in a branch::

    git checkout -b my_branch

- Install pre-commit hooks::

    pre-commit install
    pre-commit run --all-files

Ready to go.


Usage
-----

You can run the complete pipeline.::

    ./pipeline.sh

The complete pipeline includes data loading, data cleaning, training a preliminary model, using the preliminary
to limit the number of features, training a final model, and calculating integrated gradients for the features of the
final model.

This creates everything in order to be able to to run the Air Quality Prediction streamlit app.::

    streamlit run src/airqualitymodel/app.py

Heroku App
//////////

It's possible to host the app on Heroku.

1. Create a Heroku account.
2. Install ``heroku`` command line tool on your system and login. `Instructions. <https://devcenter.heroku.com/articles/heroku-cli>`__
3. Run the following commands.::

    rm -rf ./build && mkdir -p build && cd build
    heroku apps:create [app_name]
    # this creates a heroku git repo in build/ directory
    git init
    heroku git:remote -a [app_name]
    cp ../heroku/* .
    mkdir airqualitymodel data models
    cp ../src/airqualitymodel/*.py airqualitymodel/ \
    cp -r ../data/cleaned data/ \
    cp ../models/airqualitymodel.sav models/ \
    git add .
    git commit -m "heroku deployment"
    git push heroku master

The app should then be ready to go at http://[app_name].herokuapp.com/.

The commands to update an existing app are a bit different. They can be found in ``./build.sh``.


Approach
--------

Our approach follows this sequence:

1. **Data Processing**. Read raw data, combine data sources, clean the data, and save the cleaned dataset.
2. **Training the preliminary model.** Train a preliminary model with all the features.
3. **Feature Selection**. Remove highly correlated features as well as unimportant features to the preliminary model.
4. **Training the final model**. Train the final model with the reduced feature set.
5. **Explain the model predictions**. We use a method called Integrated Gradients.
6. **Visualize the results**. Use Streamlit to create a intearctive web app to present our findings.
